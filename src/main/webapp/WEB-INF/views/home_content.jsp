<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="col-sm-10 col-md-10">
    <div class="well">
        <h1><fmt:message key="label.welcome"/></h1>
        <strong><fmt:message key="label.brand"/></strong>
    </div>
</div>
<div class="col-sm-10 col-md-10">
    <div>
        <h3><fmt:message key="label.top"/></h3>
    </div>
    <table class="table">
        <thead>
        <tr>
            <%--<th>#</th>--%>
            <th><fmt:message key="label.title"/></th>
            <th><fmt:message key="label.answer"/></th>
            <th><fmt:message key="label.userName"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="question" items="${questions}">
            <tr>
                    <%--<th scope="row">1</th>--%>
                <td><a href="pages/question?id=${question.id}">${question.title}</a></td>
                <td>${question.answers.size()}</td>
                <td><a href="pages/userProfile?id=${question.user.id}">${question.user.userName}</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>