<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="utils" uri="http://utils" %>

<div class="container">

    <div class="row">
        <div class="col-sm-8">
            <div class="well">
                <h4><fmt:message key="label.askQuestion"/> </h4>
                <form role="form" action="pages/createQuestion" method="post">
                        <input type="hidden" name="id" value="${question.id}"/>
                        <input type="hidden" name="userId" value="${authenticatedUser.id}"/>
                    <div class="form-group" >
                        <input class="form-control" placeholder="<fmt:message key="label.questionTitle"/>"
                               name="title" type="text" required/>
                    </div>
                    <div class="form-group">
                        <textarea required class="form-control" name="content" rows="3" placeholder="<fmt:message key="label.questionContent"/>"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary"><fmt:message key="label.submit"/></button>
                </form>
            </div>
        </div>
    </div>
</div>