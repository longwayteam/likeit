<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="utils" uri="/WEB-INF/tld/utils.tld" %>

<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <div class="span8">
                <h1>${question.title}</h1>
                <p>${question.content}</p>
                <div>
                    <span class="badge badge-success">${utils:formatLocalDateTime(question.createdAt, 'dd.MM.yyyy HH:mm:ss')}</span>
                    <fmt:message key="label.askedBy"/> <span
                        class="badge badge-success"><a class="link"
                                                       href="pages/userProfile?id=${question.user.id}">${question.user.userName}</a></span>
                </div>
                <hr>
            </div>

        </div>
        <c:forEach var="answer" items="${question.answers}">
            <div class="col-sm-8">
                <div class="panel panel-white post panel-shadow">
                    <div class="post-heading">
                        <div class="pull-left image">
                            <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar"
                                 alt="user profile image">
                        </div>
                        <div class="pull-left meta">
                            <div class="title h5">
                                <a href="pages/userProfile?id=${answer.user.id}"><b>${answer.user.userName}</b></a>
                                    <%--<fmt:message key="label.time"/>--%>
                            </div>
                            <h6 class="text-muted time">${utils:formatLocalDateTime(answer.createdAt, 'dd.MM.yyyy HH:mm:ss')}</h6>
                        </div>
                        <div class="pull-right">
                            <c:set var="wasLiked" scope="page"
                                   value="${answer.usersWhoLiked.contains(authenticatedUser)}"/>
                            <form action="<utils:yesno test="${wasLiked}" yes="pages/deleteLike" no="pages/addLike"/>"
                                  method="post">
                                <input name="answerId" type="hidden" value="${answer.id}"/>
                                <input name="userId" type="hidden" value="${authenticatedUser.id}"/>
                                <input name="questionId" type="hidden" value="${question.id}"/>
                                <button class="btn <c:if test="${wasLiked}">btn-primary</c:if> stat-item"
                                        <c:if test="${authenticatedUser == null}">disabled="disabled"</c:if>>
                                    <i class="fa fa-thumbs-up icon"></i>
                                        ${answer.usersWhoLiked.size()}
                                </button>
                            </form>
                        </div>
                        <c:if test="${authenticatedUser.role == 'ADMIN'}">
                            <div class="pull-right">
                                <form action="pages/deleteAnswer" method="post"
                                      name="deleteAnswer">
                                    <input type="hidden" value="${answer.id}" name="answerId"/>
                                    <input type="hidden" value="${question.id}" name="questionId"/>
                                    <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal"
                                            data-target="#delete"><span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </form>
                            </div>
                        </c:if>
                    </div>
                    <div class="post-description">
                        <p>${answer.content}</p>
                    </div>
                </div>
            </div>
        </c:forEach>

        <c:if test="${authenticatedUser != null}">
            <div class="col-sm-8">
                <div class="well">
                    <h4><fmt:message key="label.leaveAnswer"/></h4>
                    <form role="form" action="pages/createAnswer" method="post">
                        <input type="hidden" name="userId" value="${authenticatedUser.id}"/>
                        <input type="hidden" name="questionId" value="${question.id}"/>
                        <div class="form-group">
                            <textarea class="form-control" name="content" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary"><fmt:message key="label.submit"/></button>
                    </form>
                </div>
            </div>
        </c:if>
    </div>
</div>



