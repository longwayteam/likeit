<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="col-sm-10 col-md-10 <c:if test="true">col-md-offset-1</c:if>">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                Questions
                <strong class="text-right">(1 / 10)</strong>
            </h3>
        </div>
        <table class="table table-hover" id="task-table">
            <thead>
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Content</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="question" items="${questions}">
                <tr>
                    <td class="text-center">
                        <a href="/pages/questions/${question.id}">${question.id}</a>
                    </td>
                    <td class="text-center">${question.content}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>