<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utils" uri="/WEB-INF/tld/utils.tld" %>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4><fmt:message key="label.askedBy"/> ${user.userName}</h4>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <th><fmt:message key="label.title"/></th>
                    <th><fmt:message key="label.user"/></th>
                    <th><fmt:message key="label.answer"/></th>
                    <th><fmt:message key="label.createdAt"/></th>
                    <th><fmt:message key="label.updatedAt"/></th>
                    <c:if test="${authenticatedUser.role == 'ADMIN'}">
                        <th><fmt:message key="label.edit"/></th>
                        <th><fmt:message key="label.delete"/></th>
                    </c:if>
                    </thead>
                    <tbody>
                    <c:forEach var="question" items="${questions}">
                        <tr>
                            <td><a href="pages/question?id=${question.id}">${question.title}</a></td>
                            <td><a href="pages/userProfile?id=${question.userId}">${question.user.userName}</a></td>
                            <td>${question.answers.size()}</td>
                            <td>${utils:formatLocalDateTime(question.createdAt, 'dd.MM.yyyy HH:mm:ss')}</td>
                            <td>${utils:formatLocalDateTime(question.updatedAt, 'dd.MM.yyyy HH:mm:ss')}</td>

                            <c:if test="${authenticatedUser.role == 'ADMIN'}">
                                <td>
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <a href="pages/updateQuestionForm?id=${question.id}">
                                            <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal"
                                                    data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
                                            <%--<form action="pages/updateQuestion?id=${question.id}" method="get">--%>

                                            <%----%>
                                            <%--</form>--%>
                                    </p>
                                </td>
                                <td>
                                    <p data-placement="top" data-toggle="tooltip" title="Delete">
                                    <form action="pages/deleteQuestion" method="post"
                                          name="deleteQuestion">
                                        <input value="${question.id}" name="questionId" type="hidden"/>
                                        <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal"
                                                data-target="#delete"><span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </form>
                                    </p>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>

                </table>

                <%--<div class="clearfix"></div>--%>
                <%--<ul class="pagination pull-right">--%>
                    <%--<li <c:if test="${!page.isPrevious()}">class="disabled"</c:if>>--%>
                        <%--<a href="pages/allQuestions?page=${page.currentPageNumber - 1}"><span class="glyphicon glyphicon-chevron-left"></span></a>--%>
                    <%--</li>--%>
                    <%--<c:forEach var="i" begin="1" end="${page.totalPages}">--%>
                        <%--<li <c:if test="${i == page.currentPageNumber}"> class="active"</c:if>>--%>
                            <%--<a href="pages/allQuestions?page=${i}">${i}</a>--%>
                        <%--</li>--%>
                    <%--</c:forEach>--%>
                    <%--<li <c:if test="${!page.isNext()}">class="disabled"</c:if>>--%>
                        <%--<a href="pages/allQuestions?page=${page.currentPageNumber + 1}"><span class="glyphicon glyphicon-chevron-right"></span></a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</div>--%>

        </div>
    </div>
</div>