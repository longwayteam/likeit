<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utils" uri="/WEB-INF/tld/utils.tld" %>

<h3>User profile</h3>
<ul class="list-group">
    <c:if test="${user.userName != null}">
        <li><fmt:message key="label.userName"/> : ${user.userName}</li>
    </c:if>
    <c:if test="${user.firstName != null}">
        <li><fmt:message key="label.firstName"/> : ${user.firstName}</li>
    </c:if>
    <c:if test="${user.lastName != null}">
        <li><fmt:message key="label.lastName"/> : ${user.lastName}</li>
    </c:if>
    <c:if test="${user.rating != null}">
        <li><fmt:message key="label.rating"/> : ${user.rating}</li>
    </c:if>
    <li><fmt:message key="label.createdAt"/> : ${utils:formatLocalDateTime(user.createdAt, 'dd.MM.yyyy HH:mm:ss')}</li>
    <li><fmt:message key="label.updatedAt"/> : ${utils:formatLocalDateTime(user.updatedAt, 'dd.MM.yyyy HH:mm:ss')}</li>
</ul>
<c:if test="${authenticatedUser != null && authenticatedUser.id == user.id}">
    <p><a href="pages/userDetail?id=${authenticatedUser.id}"><fmt:message key="label.editDetails"/></a></p>
</c:if>
<p><a href="pages/getUserQuestions?id=${user.id}"><fmt:message key="label.showUserQuestions"/></a></p>
