<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="utils" uri="/WEB-INF/tld/utils.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="container">
    <div class="row">
        <div class="col-sm-8">

            <form class="form-horizontal" method="POST" role="form"
                  action="<utils:yesno test="${not empty user.id}" yes="pages/updateUser" no="pages/createUser" />">

                <c:if test="${not empty user.id}">
                    <input type="hidden" name="id" value="${user.id}"/>
                    <input type="hidden" name="role" value="${user.role}"/>
                    <input type="hidden" name="rating" value="${user.rating}"/>
                </c:if>

                <fieldset>
                    <legend><fmt:message key="label.userDetails"/></legend>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="userName">
                            <fmt:message key="label.userName"/>
                        </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="userName" name="userName"
                                   value="${user.userName}"/>
                            <c:if test="${errors.containsKey('emptyUsername')}">
                                <span class="help-block"><fmt:message key="${errors.get('emptyUsername')}"/><br></span>
                            </c:if>
                            <c:if test="${errors.containsKey('existsUsername')}">
                                <span class="help-block"><fmt:message key="${errors.get('existsUsername')}"/><br></span>
                            </c:if>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputPassword">
                            <fmt:message key="label.password"/>
                        </label>

                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="inputPassword"
                                   name="password"/>
                            <c:if test="${errors.containsKey('password')}">
                                <span class="help-block"><fmt:message key="${errors.get('password')}"/><br></span>
                            </c:if>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputPassword">
                            <fmt:message key="label.firstName"/>
                        </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="${user.firstName}" id="firstName"
                                   name="firstName"/>
                            <%--<c:if test="${errors.containsKey('password')}">--%>
                                <%--<span class="help-block"><fmt:message key="${errors.get('password')}"/><br></span>--%>
                            <%--</c:if>--%>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputPassword">
                            <fmt:message key="label.lastName"/>
                        </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="${user.lastName}" id="lastName"
                                   name="lastName"/>
                            <%--<c:if test="${errors.containsKey('password')}">--%>
                            <%--<span class="help-block"><fmt:message key="${errors.get('password')}"/><br></span>--%>
                            <%--</c:if>--%>
                        </div>
                    </div>
                </fieldset>

                <div class="col-sm-8">
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success">
                                    <fmt:message key="label.submit"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>