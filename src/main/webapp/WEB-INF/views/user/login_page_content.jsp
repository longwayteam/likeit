<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><fmt:message key="label.pleaseLogin"/></h3>
        </div>
        <div class="panel-body">
            <form accept-charset="UTF-8" method="POST" role="form" action="/pages/login">
                <fieldset>
                    <div class="form-group <c:if test="${not empty errors}">has-error</c:if>">
                        <input class="form-control" placeholder="<fmt:message key="label.userName"/>"
                               name="username" type="text"/>

                            <c:if test="${errors.containsKey('username')}">
                                <span class="help-block"><fmt:message key="${errors.get('username')}" /><br></span>
                            </c:if>

                    </div>
                    <div class="form-group <c:if test="${not empty errors}">has-error</c:if>">
                        <input class="form-control" placeholder="<fmt:message key="label.password"/>"
                               name="password" type="password"/>

                        <c:if test="${errors.containsKey('general')}">
                            <span class="help-block"><fmt:message key="${errors.get('general')}" /><br></span>
                        </c:if>
                    </div>
                    <input class="btn btn-lg btn-success btn-block" type="submit" value="<fmt:message key="label.login"/> "/>
                </fieldset>
            </form>
        </div>
    </div>
</div>