<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<header class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/pages/home">Like IT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="false">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/pages/home"><fmt:message key="label.home"/></a></li>
                <li><a href="/pages/allQuestions"><fmt:message key="label.allQuestions"/></a></li>
                <c:if test="${authenticatedUser != null}">
                    <li><a href="/pages/askQuestion"><fmt:message key="label.askQuestion"/></a></li>
                </c:if>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        En <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="/pages/changeLanguage?languageTag=en-US"><fmt:message key="label.english"/> </a>
                        </li>
                        <li>
                            <a href="/pages/changeLanguage?languageTag=ru-RU"><fmt:message key="label.russian"/></a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${authenticatedUser == null}">
                        <li><a href="/pages/signup"><fmt:message key="label.singUp"/> </a></li>
                        <li><a href="/pages/login"><fmt:message key="label.signIn"/> </a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="/pages/userProfile?id=${authenticatedUser.id}" class="list-group-item">
                            <i class="fa fa-comment-o"></i>
                            <fmt:message key="label.myProfile"/>
                        </a></li>
                        <li><a href="/pages/getUserQuestions?id=${authenticatedUser.id}" class="list-group-item">
                            <i class="fa fa-search"></i>
                            <fmt:message key="label.myQuestions"/>
                        </a></li>
                        <li>
                            <form method="post" action="/pages/logout">
                                <button type="submit" class="btn">
                                        <%--<span class="glyphicon glyphicon-share-alt"></span>--%>
                                    <fmt:message key="label.logout"/>
                                </button>
                            </form>
                            </form> </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</header>