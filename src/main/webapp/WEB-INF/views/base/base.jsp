<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Questions</title>

    <base href="${pageContext.request.contextPath}/">

    <!-- Bootstrap -->
    <link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="resources/css/sticky-footer-navbar.css" rel="stylesheet">

    <link href="resources/css/comments.css" rel="stylesheet">

    <link href="resources/css/style.css" rel="stylesheet">

    <script src="resources/js/jquery/1.11.1/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>

    <script src="resources/js/model_panel.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<jsp:include page="header.jsp"/>

<article class="container">
    <div class="row">
        <%--<jsp:include page="aside.jsp"/>--%>

        <jsp:include page="/WEB-INF/views/${param.content}.jsp"/>
    </div>
</article>

<jsp:include page="footer.jsp"/>
</body>
</html>