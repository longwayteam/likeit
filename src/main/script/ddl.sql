-- -----------------------------------------------------
-- Schema like_it
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `like_it` DEFAULT CHARACTER SET utf8 ;
USE `like_it`;

-- -----------------------------------------------------
-- Table `like_it`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(128) NOT NULL UNIQUE,
  `password` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(255),
  `last_name` VARCHAR(255),
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` ENUM('ADMIN', 'USER') NOT NULL,
  `rating` INT(11) NOT NULL DEFAULT 0,

  CONSTRAINT `user_id_pk` PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARACTER SET = utf8;
    
-- -----------------------------------------------------
-- Table `like_it`.`questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`question` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` VARCHAR(4000) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` BIGINT(20) NOT NULL,

  CONSTRAINT `question_id_pk` PRIMARY KEY (`id`),
  CONSTRAINT `question_user_id_fk` FOREIGN KEY(`user_id`) REFERENCES `like_it`.`user`(`id`)
)
  ENGINE = InnoDB
	DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `like_it`.`answers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `like_it`.`answer` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(4000) NOT NULL,
  `rating` INT(11) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` BIGINT(20) NOT NULL,
  `question_id` BIGINT(20) NOT NULL,

  CONSTRAINT `answer_id_pk` PRIMARY KEY (`id`),
  CONSTRAINT `answer_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `like_it`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `answer_question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `like_it`.`question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)
	ENGINE = InnoDB
	DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `like_it`.`like` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `answer_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,

  CONSTRAINT `like_id_pk` PRIMARY KEY (`id`),
  CONSTRAINT `like_answer_id_fk` FOREIGN KEY (`answer_id`) REFERENCES `like_it`.`answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `like_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `like_it`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;