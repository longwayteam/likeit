package org.likeit.core;

public class Err {

    private final int code;
    private final String message;

    public Err(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
