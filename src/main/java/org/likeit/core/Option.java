package org.likeit.core;

import java.util.Collections;
import java.util.List;

public class Option<T> {

    private T object;

    private List<Err> errors;

    private Option(T object) {
        this.object = object;
    }

    private Option(List<Err> errors) {
        this.errors = errors;
    }

    public static <T> Option <T> success(T object) {
        return new Option<>(object);
    }

    public static <T> Option <T> error(Err error) {
        return error(Collections.singletonList(error));
    }

    public static <T> Option <T> error(List<Err> errors) {
        return new Option<>(errors);
    }

    public T getObject() {
        return object;
    }

    public List<Err> getErrors() {
        if (errors == null) {
            return Collections.emptyList();
        }

        return errors;
    }

    public boolean hasErrors() {
        return !getErrors().isEmpty();
    }

    public boolean isSuccess() {
        return errors.isEmpty();
    }

}
