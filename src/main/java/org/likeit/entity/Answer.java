package org.likeit.entity;

import java.util.Collections;
import java.util.List;

public class Answer extends  Entity {

    private long rating;
    private String content;
    private long questionId;
    private User user;
    private List<User> usersWhoLiked;

    public Answer() {
        super();
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsersWhoLiked() {
        if(usersWhoLiked == null) {
            return Collections.emptyList();
        }
        return usersWhoLiked;
    }

    public void setUsersWhoLiked(List<User> usersWhoLiked) {
        this.usersWhoLiked = usersWhoLiked;
    }
}
