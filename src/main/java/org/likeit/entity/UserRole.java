package org.likeit.entity;

public enum UserRole {
    USER,
    ADMIN
}
