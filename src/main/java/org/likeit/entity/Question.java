package org.likeit.entity;

import java.util.Collections;
import java.util.List;

public class Question extends Entity {

    private String title;
    private String content;
    private long userId;
    private User user;
    private List<Answer> answers;

    public Question() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Answer> getAnswers() {

        if(answers == null) {
            return Collections.emptyList();
        }

        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}