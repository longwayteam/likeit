package org.likeit.dao;

import org.likeit.entity.Question;

import java.util.List;

public interface QuestionDao extends Dao<Question> {
    List<Question> getLimit(Long offSet, Long limit);

    List<Question> getUserQuestion(Long offSet, Long limit, Long userId);
}
