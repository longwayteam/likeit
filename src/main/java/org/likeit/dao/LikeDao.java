package org.likeit.dao;

import org.likeit.entity.Like;

import java.util.List;

public interface LikeDao extends Dao<Like> {
    List<Like> getAnswerLikes(Long answerId);

    void delete(Long userId, Long answerId);
}
