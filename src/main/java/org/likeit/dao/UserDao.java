package org.likeit.dao;

import org.likeit.entity.User;

public interface UserDao extends Dao<User> {

    User findByUserName(String userName);

}
