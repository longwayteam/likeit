package org.likeit.dao;

import org.likeit.entity.Entity;
import org.likeit.entity.User;

import java.util.List;

public interface Dao<T extends Entity> {

    List<T> getAll();

    T findById(Long id);

    long create(T entity);

    void delete(Long id);

    long update(T entity);

    Long getCountOfEntities();
}

