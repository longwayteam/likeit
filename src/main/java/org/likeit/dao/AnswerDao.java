package org.likeit.dao;

import org.likeit.entity.Answer;

import java.util.List;

public interface AnswerDao extends Dao<Answer> {
    List<Answer> getAnswersByQuestionId(long questionId);
}
