package org.likeit.dao.impl;

import org.apache.log4j.Logger;
import org.likeit.dao.AnswerDao;
import org.likeit.dao.DaoException;
import org.likeit.dao.UserDao;
import org.likeit.dao.util.Criteria;
import org.likeit.dao.util.Restriction;
import org.likeit.entity.Answer;
import org.likeit.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnswerDaoImpl extends GenericDaoImpl<Answer> implements AnswerDao {

    private UserDao userDao;

    public AnswerDaoImpl() {
        this.userDao = new UserDaoImpl();
    }

    private static final Logger log = Logger.getLogger(AnswerDaoImpl.class);

    @Override
    protected Answer fromResultSet(ResultSet resultSet) {
        try {
            Answer answer = new Answer();

            answer.setId(resultSet.getLong("id"));
            answer.setContent(resultSet.getString("content"));
            answer.setQuestionId(resultSet.getLong("question_id"));
            answer.setRating(resultSet.getLong("rating"));

            User user = userDao.findById(resultSet.getLong("user_id"));

            user.setId(user.getId());

            answer.setUser(user);
            answer.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
            answer.setUpdatedAt(resultSet.getTimestamp("updated_at").toLocalDateTime());

            return answer;

        } catch (SQLException e) {
            log.error("Exception happen", e);
            throw new DaoException("Result set is invalid.", e);
        }
    }

    @Override
    protected Map<String, String> fromEntity(Answer entity) {

        Map<String, String> map = new HashMap<>();

//        map.put("id", Long.toString(entity.getId()));
        map.put("content", entity.getContent());
        map.put("user_id", Long.toString(entity.getUser().getId()));
        map.put("question_id", Long.toString(entity.getQuestionId()));
        map.put("rating", Long.toString(entity.getRating()));

        return map;
    }

    @Override
    protected String getTableName() {
        return "answer";
    }

    @Override
    public List<Answer> getAnswersByQuestionId(long questionId) {

        Criteria criteria = new Criteria();
        criteria.add(Restriction.eq("question_id", questionId));

        return findListByCriteria(criteria);
    }
}
