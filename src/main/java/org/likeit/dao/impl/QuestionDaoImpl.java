package org.likeit.dao.impl;

import org.apache.log4j.Logger;
import org.likeit.dao.DaoException;
import org.likeit.dao.QuestionDao;
import org.likeit.dao.UserDao;
import org.likeit.dao.util.Criteria;
import org.likeit.dao.util.Restriction;
import org.likeit.entity.Question;
import org.likeit.entity.User;
import org.likeit.service.impl.QuestionServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionDaoImpl extends GenericDaoImpl<Question> implements QuestionDao {

    private static final Logger log = Logger.getLogger(QuestionServiceImpl.class);

    private UserDao userDao;

    public QuestionDaoImpl() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public List<Question> getUserQuestion(Long offSet, Long limit, Long userId) {
        Criteria criteria = new Criteria();
        criteria.setLimit(limit);
        criteria.setOffSet(offSet);
        criteria.add(Restriction.eq("user_id", userId));

        List<Question> questions = findListByCriteria(criteria);

        return questions;
    }

    @Override
    public List<Question> getLimit(Long offSet, Long limit) {
        Criteria criteria = new Criteria();

        criteria.setLimit(limit);
        criteria.setOffSet(offSet);

        List<Question> questions = findListByCriteria(criteria);

        return questions;
    }

    @Override
    protected Question fromResultSet(ResultSet resultSet) {
        try {
            Question question = new Question();

            question.setId(resultSet.getLong("id"));
            question.setTitle(resultSet.getString("title"));
            question.setContent(resultSet.getString("content"));

            User user = userDao.findById(resultSet.getLong("user_id"));
            question.setUser(user);
            question.setUserId(resultSet.getInt("user_id"));
            question.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
            question.setUpdatedAt(resultSet.getTimestamp("updated_at").toLocalDateTime());

            return question;

        } catch (SQLException e) {
            log.error("Exception happen", e);
            throw new DaoException("Result set is invalid.", e);
        }

    }

    @Override
    protected Map<String, String> fromEntity(Question question) {
        Map<String, String> map = new HashMap<>();

        map.put("title", question.getTitle());
        map.put("content", question.getContent());
        map.put("user_id", Long.toString(question.getUserId()));

        return map;
    }

    @Override
    protected String getTableName() {
        return "question";
    }

}
