package org.likeit.dao.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.likeit.dao.Dao;
import org.likeit.dao.DaoException;
import org.likeit.dao.util.ConnectionManager;
import org.likeit.dao.util.Criteria;
import org.likeit.dao.util.Restriction;
import org.likeit.entity.Entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class GenericDaoImpl<T extends Entity> implements Dao<T> {

    private static final Logger log = Logger.getLogger(GenericDaoImpl.class);

    private ConnectionManager connectionManager = ConnectionManager.getInstance();

    @Override
    public List<T> getAll() {
        return findListByCriteria(null);
    }

    @Override
    public T findById(Long id) {
        Criteria criteria = new Criteria();
        criteria.add(Restriction.eq("id", id));

        return findListByCriteria(criteria).get(0);
    }

    @Override
    public long create(T entity) {

        Connection connection = connectionManager.getConnection();

        Map<String, String> map = fromEntity(entity);

        List<String> keys = new ArrayList<>(map.keySet());
        List<String> question_marks = new ArrayList<>();
        List<String> values = new ArrayList<>();

        for (String key : keys) {
            values.add(map.get(key));
            question_marks.add("?");
        }

        String sql = "INSERT INTO `" + getTableName() +
                "` (" + StringUtils.join(keys, ", ") + ") VALUES(" + StringUtils.join(question_marks, ", ") + ")";

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            setValues(statement, values);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Inserting user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                } else {

                    throw new SQLException("Inserting user failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            log.error("Exceptions happen!" + sql, e);
            throw new DaoException(e);
        } finally {
            connectionManager.returnConnection(connection);
        }
    }

    @Override
    public void delete(Long id) {

        String sql = String.format("DELETE FROM `%s` WHERE id=?", getTableName());

        Connection connection = connectionManager.getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Exception happen!", e);
            throw new DaoException(e);
        } finally {
            connectionManager.returnConnection(connection);
        }

    }

    @Override
    public Long getCountOfEntities() {

        String sql = "SELECT COUNT(*) FROM `" + getTableName() + "`";

        Connection connection = connectionManager.getConnection();

        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();

            return resultSet.getLong(1);

        } catch (SQLException e) {
            log.error("Exception happen!", e);
            throw new DaoException(e);
        } finally {
            connectionManager.returnConnection(connection);
        }
    }

    @Override
    public long update(T entity) {

        Connection connection = connectionManager.getConnection();

        Map<String, String> map = fromEntity(entity);

        List<String> couples = new ArrayList<>();
        List<String> values = new ArrayList<>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            values.add(entry.getValue());
            couples.add(String.format("`%s`=?", entry.getKey()));
        }

        String sql = "UPDATE " + getTableName() +
                " SET " + StringUtils.join(couples, ", ") +
                " WHERE id=?";

        values.add(Long.toString(entity.getId()));

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            setValues(statement, values);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Updating user failed, no rows affected.");
            }
            return entity.getId();
        } catch (SQLException e) {
            log.error("Exceptions happen!", e);
            throw new DaoException(e);
        } finally {
            connectionManager.returnConnection(connection);
        }
    }

    protected T findOneByCriteria(Criteria criteria) {
        List<T> list = findListByCriteria(criteria);

        if (list.isEmpty()) {
            return null;
        }
        if (list.size() > 1) {
            DaoException e = new DaoException("More then one row was founded");
            log.error("Exception happen!", e);
            throw e;
        }

        return list.get(0);
    }

    protected List<T> findListByCriteria(Criteria criteria) {
        StringBuilder sql = new StringBuilder("SELECT * FROM ");
        sql.append("`" + getTableName() + "`");

        if (criteria != null && !criteria.getRestrictions().isEmpty()) {
            List<Restriction> restrictions = criteria.getRestrictions();

            sql.append(" WHERE ");
            for (int i = 0; i < restrictions.size(); i++) {
                Restriction restriction = restrictions.get(i);
                if (i != 0) {
                    sql.append(" AND ");
                }
                sql.append(restriction.getColumnName() + restriction.getOperator() + "'" + restriction.getColumnValue() + "'");
            }
        }

        if(criteria != null && criteria.getOffSet() != null && criteria.getLimit() != null) {
            sql.append(" LIMIT " + criteria.getOffSet() +"," + criteria.getLimit());

        }

        Connection connection = connectionManager.getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql.toString())) {
            ResultSet resultSet = statement.executeQuery();

            List<T> entities = new ArrayList<>();
            while (resultSet.next()) {
                entities.add(fromResultSet(resultSet));
            }

            return entities;
        } catch (SQLException e) {
            log.error("Exception happen!", e);
            throw new DaoException(e);
        } finally {
            connectionManager.returnConnection(connection);
        }
    }

    protected void deleteByCriteria(Criteria criteria) {
        StringBuilder sql = new StringBuilder("DELETE FROM ");
        sql.append("`" + getTableName() + "`");

        if (criteria != null && !criteria.getRestrictions().isEmpty()) {
            List<Restriction> restrictions = criteria.getRestrictions();

            sql.append(" WHERE ");
            for (int i = 0; i < restrictions.size(); i++) {
                Restriction restriction = restrictions.get(i);
                if (i != 0) {
                    sql.append(" AND ");
                }
                sql.append(restriction.getColumnName() + restriction.getOperator() + "'" + restriction.getColumnValue() + "'");
            }
        }

        Connection connection = connectionManager.getConnection();

        try (PreparedStatement statement = connection.prepareStatement(sql.toString())) {
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Exception happen!", e);
            throw new DaoException(e);
        } finally {
            connectionManager.returnConnection(connection);
        }
    }

    protected abstract T fromResultSet(ResultSet resultSet);

    protected abstract Map<String, String> fromEntity(T entity);

    protected abstract String getTableName();

    private void setValues(PreparedStatement statement, List<String> values) throws SQLException {
        for (int i = 0; i < values.size(); i++) {
            statement.setObject(i + 1, values.get(i));
        }
    }

}
