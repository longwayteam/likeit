package org.likeit.dao.impl;

import org.apache.log4j.Logger;
import org.likeit.dao.DaoException;
import org.likeit.dao.UserDao;
import org.likeit.dao.util.Criteria;
import org.likeit.dao.util.Restriction;
import org.likeit.entity.UserRole;
import org.likeit.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    private static final Logger log = Logger.getLogger(UserDaoImpl.class);

    @Override
    protected User fromResultSet(ResultSet resultSet) {

        User user = new User();

        try {
            user.setId(resultSet.getLong("id"));
            user.setUserName(resultSet.getString("user_name"));
            user.setPassword(resultSet.getString("password"));
            user.setRating(resultSet.getLong("rating"));
            user.setRole(UserRole.valueOf(resultSet.getString("role")));
            user.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
            user.setUpdatedAt(resultSet.getTimestamp("updated_at").toLocalDateTime());
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));

            return user;
        } catch (SQLException e) {
            log.error("Exception happen", e);
            throw new DaoException(e);
        }
    }

    @Override
    protected Map<String, String> fromEntity(User entity) {

        Map<String, String> map = new HashMap<>();

        map.put("user_name", entity.getUserName());
        map.put("password", entity.getPassword());
        map.put("first_name", entity.getFirstName());
        map.put("last_name", entity.getLastName());
        map.put("role", entity.getRole().name());
        map.put("rating", Long.toString(entity.getRating()));

        return map;
    }

    @Override
    protected String getTableName() {
        return "user";
    }

    @Override
    public User findByUserName(String userName) {
        Criteria criteria = new Criteria();
        criteria.add(Restriction.eq("user_name", userName));

        return findOneByCriteria(criteria);
    }
}
