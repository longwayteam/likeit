package org.likeit.dao.impl;

import org.apache.log4j.Logger;
import org.likeit.dao.DaoException;
import org.likeit.dao.LikeDao;
import org.likeit.dao.util.Criteria;
import org.likeit.dao.util.Restriction;
import org.likeit.entity.Like;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LikeDaoImpl extends GenericDaoImpl<Like> implements LikeDao {


    private static Logger logger = Logger.getLogger(LikeDaoImpl.class);

    @Override
    protected Like fromResultSet(ResultSet resultSet) {
        try {
            Like like = new Like();

            like.setAnswerId(resultSet.getLong("answer_id"));
            like.setUserId(resultSet.getLong("user_id"));
            like.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
            like.setUpdatedAt(resultSet.getTimestamp("updated_at").toLocalDateTime());

            return like;

        } catch (SQLException e) {
            logger.error("Exception happen!", e);
            throw new DaoException("Result set is invalid.", e);
        }
    }

    @Override
    protected Map<String, String> fromEntity(Like entity) {
        Map<String, String> map = new HashMap<>();

        map.put("answer_id", Long.toString(entity.getAnswerId()));
        map.put("user_id", Long.toString(entity.getUserId()));

        return map;
    }

    @Override
    protected String getTableName() {
        return "like";
    }

    @Override
    public List<Like> getAnswerLikes(Long answerId) {
        Criteria criteria = new Criteria();
        criteria.add(Restriction.eq("answer_id", answerId));

        return findListByCriteria(criteria);
    }

    @Override
    public void delete(Long userId, Long answerId) {
        Criteria criteria = new Criteria();
        criteria.add(Restriction.eq("answer_id", answerId));
        criteria.add(Restriction.eq("user_id", userId));

        deleteByCriteria(criteria);
    }

}
