package org.likeit.dao.util;

import org.apache.log4j.Logger;
import org.likeit.dao.impl.AnswerDaoImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionManager {

    private static final Logger log = Logger.getLogger(ConnectionManager.class);

    private static class LazyHolder {
        private static final ConnectionManager INSTANCE = new ConnectionManager();
    }

    public static ConnectionManager getInstance() {
        return ConnectionManager.LazyHolder.INSTANCE;
    }

    private ConnectionPool connectionPool;

    private ConnectionManager() {
        ResourceBundle dbProperties = ResourceBundle.getBundle("db");

        String url = dbProperties.getString("url");
        String driver = dbProperties.getString("driver");
        String user = dbProperties.getString("username");
        String password = dbProperties.getString("password");
        int capacity = Integer.parseInt(dbProperties.getString("capacity"));

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            log.error("Driver class '" + driver + "' is missing in classpath.", e);

            throw new RuntimeException("Driver class '" + driver + "' is missing in classpath.", e);
        }

        connectionPool = new ConnectionPool(capacity);

        for (int i = 0; i < capacity; i++) {
            connectionPool.add(newConnection(url, user, password));
        }
    }

    public Connection getConnection() {
        return connectionPool.get();
    }

    public void returnConnection(Connection connection) {
        connectionPool.release(connection);
    }

    private Connection newConnection(String url, String user, String password) {
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            log.error("Error with connection to db", e);
            throw new RuntimeException(e);
        }
    }
}
