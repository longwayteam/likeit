package org.likeit.dao.util;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


class ConnectionPool {

    Logger logger = Logger.getLogger(ConnectionPool.class);

    private final BlockingQueue<Connection> pool;

    public ConnectionPool(int capacity) {
        pool = new LinkedBlockingQueue<>(capacity);
    }

    public Connection get() {
        try {
            return pool.take();
        } catch (InterruptedException e) {
            logger.error("Couldn't get a connection from the pool.", e);
            throw new RuntimeException("Couldn't get a connection from the pool.", e);
        }
    }

    public void release(Connection connection) {
        pool.offer(connection);
    }

    public void add(Connection connection) {
        pool.add(connection);
    }

}