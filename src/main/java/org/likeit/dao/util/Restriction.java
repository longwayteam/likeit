package org.likeit.dao.util;


public class Restriction {

    private String columnName;
    private String operator;
    private Object columnValue;

    private Restriction(String columnName, String operator, Object columnValue) {
        this.columnName = columnName;
        this.operator = operator;
        this.columnValue = columnValue;
    }

    public static Restriction eq(String columnName, Object columnValue) {
        return new Restriction(columnName, "=", columnValue);
    }

    public String getColumnName() {
        return columnName;
    }

    public String getOperator() {
        return operator;
    }

    public Object getColumnValue() {
        return columnValue;
    }
}
