package org.likeit.dao.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Criteria {

    private List<Restriction> restrictions;
    private Long limit;
    private Long offSet;

    public void add(Restriction restriction) {
        if (restrictions == null) {
            restrictions = new ArrayList<>();
        }

        restrictions.add(restriction);
    }

    public List<Restriction> getRestrictions() {
        if(restrictions == null){
            return Collections.emptyList();
        }

        return restrictions;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getOffSet() {
        return offSet;
    }

    public void setOffSet(Long offSet) {
        this.offSet = offSet;
    }

}
