package org.likeit.service;

import org.likeit.entity.Question;
import org.likeit.service.pagination.Page;
import org.likeit.service.pagination.PageRequest;

import java.util.List;

public interface QuestionService {

    long create(Question question);

    long update(Question question);

    Question get(long questionId);

    Page<Question> getPage(PageRequest pageRequest);

    void delete(long questionId);

    List<Question> getLimit(Long offSet, Long limit);

    List<Question> getUserQuestions(Long offSet, Long limit, Long questionId);
}
