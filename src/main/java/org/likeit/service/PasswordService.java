package org.likeit.service;

public interface PasswordService {
    String createHash(String password);
    boolean match(String password, String hashOfPassword);
}
