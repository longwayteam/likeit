package org.likeit.service;

import org.likeit.core.Err;
import org.likeit.core.Option;
import org.likeit.entity.User;

import java.util.List;

public interface UserService {

    Err ERROR_USER_NOT_FOUND = new Err(1, "User not found");
    Err ERROR_USERNAME_DOES_NOT_MATCH_PASSWORD = new Err(2, "Username does not match password");
    Err ERROR_USERNAME_ALREADY_EXISTS = new Err(3, "Username already exists");
    Err ERROR_USERNAME_IS_EMPTY = new Err(4, "Username is empty");
    Err ERROR_PASSWORD_TOO_SMALL = new Err(5, "Password must be at least 6 symbols");


    Option<User> create(User user);

    Option<User> update(User user);

    User get(long userId);

    List<User> getAll();

    void delete(long userId);

    Option<User> authenticate(String userName, String password);
}
