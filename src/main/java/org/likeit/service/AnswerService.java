package org.likeit.service;


import org.likeit.entity.Answer;
import org.likeit.entity.User;

import java.util.List;
import java.util.Map;

public interface AnswerService {

    List<Answer> getAnswersByQuestionId(long questionId);

    Map<Answer, User> getAnswersAndUsersByQuestionId(long questionId);

    long create(Answer answer);

    long update(Answer answer);

    Answer get(long answerId);

    List<Answer> getAll();

    void delete(long answerId);
}
