package org.likeit.service.pagination;

import org.likeit.entity.Entity;

import java.util.List;

public class Page<T extends Entity> {
    private List<T> entities;
    private long currentPageNumber;
    private long countOfAllEntities;
    private long entitiesPerPage;

    public Page(List<T> entities, long countOfAllEntities, PageRequest request) {
        this.currentPageNumber = request.getPageNumber();
        this.entitiesPerPage = request.getGetEntitiesPerPage();
        this.entities = entities;
        this.countOfAllEntities = countOfAllEntities;
    }

    public List<T> getEntities() {
        return entities;
    }

    public void setEntities(List<T> entities) {
        this.entities = entities;
    }

    public long getCurrentPageNumber() {
        return currentPageNumber;
    }

    public void setCurrentPageNumber(long currentPageNumber) {
        this.currentPageNumber = currentPageNumber;
    }

    public long getCountOfAllEntities() {
        return countOfAllEntities;
    }

    public void setCountOfAllEntities(long countOfAllEntities) {
        this.countOfAllEntities = countOfAllEntities;
    }

    public long getEntitiesPerPage() {
        return entitiesPerPage;
    }

    public void setEntitiesPerPage(long entitiesPerPage) {
        this.entitiesPerPage = entitiesPerPage;
    }

    public long getTotalPages() {
        if (countOfAllEntities < entitiesPerPage) {
            return 1L;
        }

        if (countOfAllEntities % entitiesPerPage == 0) {
            return countOfAllEntities / entitiesPerPage;
        }

        return countOfAllEntities / entitiesPerPage + 1;
    }

    public boolean isNext() {
        return currentPageNumber < getTotalPages();
    }

    public boolean isPrevious() {
        return currentPageNumber > 1;
    }
}
