package org.likeit.service.pagination;

public class PageRequest {

    private long pageNumber;
    private long getEntitiesPerPage;

    public PageRequest(long pageNumber, long getEntitiesPerPage) {
        this.pageNumber = pageNumber;
        this.getEntitiesPerPage = getEntitiesPerPage;
    }

    public long getPageNumber() {
        return pageNumber;
    }

    public long getGetEntitiesPerPage() {
        return getEntitiesPerPage;
    }
}