package org.likeit.service.impl;

import org.likeit.dao.AnswerDao;
import org.likeit.dao.DaoException;
import org.likeit.dao.LikeDao;
import org.likeit.dao.UserDao;
import org.likeit.dao.impl.AnswerDaoImpl;
import org.likeit.dao.impl.LikeDaoImpl;
import org.likeit.dao.impl.UserDaoImpl;
import org.likeit.entity.Answer;
import org.likeit.entity.Like;
import org.likeit.entity.User;
import org.likeit.service.AnswerService;
import org.likeit.service.ServiceException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnswerServiceImpl implements AnswerService {

    private AnswerDao answerDao;
    private LikeDao likeDao;
    private UserDao userDao;

    public AnswerServiceImpl() {
        this(new AnswerDaoImpl(), new UserDaoImpl(), new LikeDaoImpl());    }

    public AnswerServiceImpl(AnswerDao answerDao, UserDao userDao, LikeDao likeDao) {
        this.answerDao = answerDao;
        this.likeDao = likeDao;
        this.userDao = userDao;
    }

    @Override
    public List<Answer> getAnswersByQuestionId(long questionId) {

        List<Answer> answers = answerDao.getAnswersByQuestionId(questionId);

        for(Answer answer: answers) {
            List<Like> likes = likeDao.getAnswerLikes(answer.getId());
            List<User> usersWhoLiked = new ArrayList<>();

            for(Like like : likes) {
                User user = userDao.findById(like.getUserId());
                usersWhoLiked.add(user);
            }

            answer.setUsersWhoLiked(usersWhoLiked);
        }

        return answers;
    }

    @Override
    public Map<Answer, User> getAnswersAndUsersByQuestionId(long questionId) {
        Map<Answer, User> map = new HashMap<>();

        try {
            List<Answer> answerList = answerDao.getAnswersByQuestionId(questionId);
            for (Answer answer : answerList) {
                map.put(answer, userDao.findById(answer.getUser().getId()));
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return map;
    }

    @Override
    public long create(Answer answer) {

        try {
            return answerDao.create(answer);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public long update(Answer answer) {
        try {
            return answerDao.update(answer);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Answer get(long answerId) {
        try {
            Answer answer = answerDao.findById(answerId);
            User user = userDao.findById(answer.getUser().getId());

            answer.setUser(user);

            return answer;

        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Answer> getAll() {
        try {
            return answerDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(long answerId) {
        try {
            answerDao.delete(answerId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
