package org.likeit.service.impl;

import org.likeit.dao.DaoException;
import org.likeit.dao.LikeDao;
import org.likeit.dao.UserDao;
import org.likeit.dao.impl.LikeDaoImpl;
import org.likeit.dao.impl.UserDaoImpl;
import org.likeit.entity.Like;
import org.likeit.entity.User;
import org.likeit.service.LikeService;
import org.likeit.service.ServiceException;

import java.util.List;

public class LikeServiceImpl implements LikeService {

    private LikeDao likeDao;
    private UserDao userDao;

    public LikeServiceImpl() {
        this(new LikeDaoImpl(), new UserDaoImpl());
    }

    public LikeServiceImpl(LikeDao likeDao, UserDao userDao) {
        this.likeDao = likeDao;
        this.userDao = userDao;
    }

    @Override
    public void addLikeToUser(long userId) {

        User user = userDao.findById(userId);

        user.setRating(user.getRating() + 1);

        userDao.update(user);
    }

    @Override
    public void deleteLikeToUser(Like like) {
        Long userId = like.getUserId();

        User user = userDao.findById(userId);

        user.setRating(user.getRating() - 1);

        userDao.update(user);
    }

    @Override
    public long create(Like like) {
        try {
            return likeDao.create(like);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public long update(Like like) {
        try {
            return likeDao.update(like);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Like get(long likeId) {
        try {
            return likeDao.findById(likeId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Like> getAll() {
        try {
            return likeDao.getAll();
        } catch (DaoException e) {
            throw  new ServiceException(e);
        }
    }

    @Override
    public void delete(long likeId) {
        try {
            likeDao.delete(likeId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Like> getAnswerLikes(Long answerId) {
        try {
            return likeDao.getAnswerLikes(answerId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(long userId, long answerId) {
        try {
            likeDao.delete(userId, answerId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


}
