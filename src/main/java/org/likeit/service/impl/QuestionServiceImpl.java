package org.likeit.service.impl;


import org.likeit.dao.*;
import org.likeit.dao.impl.AnswerDaoImpl;
import org.likeit.dao.impl.LikeDaoImpl;
import org.likeit.dao.impl.QuestionDaoImpl;
import org.likeit.dao.impl.UserDaoImpl;
import org.likeit.entity.Answer;
import org.likeit.entity.Like;
import org.likeit.entity.Question;
import org.likeit.entity.User;
import org.likeit.service.QuestionService;
import org.likeit.service.ServiceException;
import org.likeit.service.pagination.Page;
import org.likeit.service.pagination.PageRequest;

import java.util.ArrayList;
import java.util.List;

public class QuestionServiceImpl implements QuestionService {

    private QuestionDao questionDao;
    private AnswerDao answerDao;
    private UserDao userDao;
    private LikeDao likeDao;

    public QuestionServiceImpl() {
        this(new QuestionDaoImpl(), new UserDaoImpl(), new AnswerDaoImpl(), new LikeDaoImpl());
    }

    public QuestionServiceImpl(QuestionDao questionDao, UserDao userDao, AnswerDao answerDao, LikeDao likeDao) {
        this.questionDao = questionDao;
        this.userDao = userDao;
        this.answerDao = answerDao;
        this.likeDao = likeDao;
    }

    public long createOrUpdate(Question question) {
        return question.getId() == null ? questionDao.create(question) : questionDao.update(question);
    }

    @Override
    public long create(Question question) {
        try {
            return questionDao.create(question);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public long update(Question question) {
        try {
            return questionDao.update(question);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Question get(long questionId) {
        try {
            Question question = questionDao.findById(questionId);
            User user = userDao.findById(question.getUserId());
            List<Answer> answers = answerDao.getAnswersByQuestionId(questionId);

            for (Answer answer : answers) {
                List<Like> likes = likeDao.getAnswerLikes(answer.getId());
                List<User> usersWhoLiked = new ArrayList<>();

                for (Like like : likes) {
                    User user1 = userDao.findById(like.getUserId());
                    usersWhoLiked.add(user1);
                }

                answer.setUsersWhoLiked(usersWhoLiked);
            }

            question.setUser(user);
            question.setAnswers(answers);

            return question;

        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<Question> getPage(PageRequest pageRequest) {

        try {
            long countOfAllEntities = questionDao.getCountOfEntities();
            long offSet = 0;

            if (pageRequest.getPageNumber() > 1 && countOfAllEntities > pageRequest.getGetEntitiesPerPage())
                offSet = pageRequest.getGetEntitiesPerPage() * (pageRequest.getPageNumber() - 1);

            List<Question> questions = getLimit(offSet, pageRequest.getGetEntitiesPerPage());

            Page<Question> page = new Page<>(questions, countOfAllEntities, pageRequest);

            return page;


        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(long questionId) {
        try {
            questionDao.delete(questionId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Question> getLimit(Long offSet, Long limit) {
        try {
            List<Question> questions = questionDao.getLimit(offSet, limit);

            for (Question question : questions) {
                question.setAnswers(answerDao.getAnswersByQuestionId(question.getId()));
            }

            return questions;

        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Question> getUserQuestions(Long offSet, Long limit, Long questionId) {
        try {
            List<Question> questions = questionDao.getUserQuestion(offSet, limit, questionId);

            return questions;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}

