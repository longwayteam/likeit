package org.likeit.service.impl;

import org.likeit.core.Err;
import org.likeit.core.Option;
import org.likeit.dao.DaoException;
import org.likeit.dao.UserDao;
import org.likeit.dao.impl.UserDaoImpl;
import org.likeit.entity.User;
import org.likeit.service.PasswordService;
import org.likeit.service.ServiceException;
import org.likeit.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao;
    private PasswordService passwordService;

    public UserServiceImpl() {
        this(new UserDaoImpl(), new PasswordServiceImpl());
    }

    public UserServiceImpl(UserDao userDao, PasswordService passwordService) {
        this.userDao = userDao;
        this.passwordService = passwordService;
    }

    @Override
    public Option<User> create(User user) {
        List<Err> errs = validateUser(user);

        if (!errs.isEmpty()) {
            return Option.error(errs);
        }

        try {
            String hash = passwordService.createHash(user.getPassword());
            user.setPassword(hash);
            long userId = userDao.create(user);
            user.setId(userId);
        } catch (DaoException e) {
            throw new ServiceException("Exception happen", e);
        }

        return Option.success(user);
    }

    private List<Err> validateUser(User user) {
        User findUser = null;
        try {
            findUser = userDao.findByUserName(user.getUserName());
        } catch (DaoException e) {
            throw new ServiceException("Exception happen!", e);
        }

        List<Err> errs = new ArrayList<>();

        if (findUser != null && !findUser.equals(user)) {
            errs.add(ERROR_USERNAME_ALREADY_EXISTS);
        }

        if (user.getUserName().isEmpty()) {
            errs.add(ERROR_USERNAME_IS_EMPTY);
        }

        if (user.getPassword().length() < 6) {
            errs.add(ERROR_PASSWORD_TOO_SMALL);
        }

        return errs;
    }

    @Override
    public Option<User> update(User user) {
        List<Err> errs = validateUser(user);

        if (!errs.isEmpty()) {
            return Option.error(errs);
        }

        try {
            user.setPassword(passwordService.createHash(user.getPassword()));
            userDao.update(user);
        } catch (DaoException e) {
            throw new ServiceException("Exception happen", e);
        }

        return Option.success(user);
    }

    @Override
    public User get(long userId) {
        try {
            return userDao.findById(userId);
        } catch (DaoException e) {
            throw new ServiceException("Exception happen", e);
        }
    }

    @Override
    public List<User> getAll() {
        try {
            return userDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException("Exception happen", e);
        }
    }

    @Override
    public void delete(long userId) {
        try {
            userDao.delete(userId);
        } catch (DaoException e) {
            throw new ServiceException("Exception happen", e);
        }
    }

    @Override
    public Option<User> authenticate(String userName, String password) {
        User user = null;

        try {
            user = userDao.findByUserName(userName);
        } catch (DaoException e) {
            throw new ServiceException("Exception happen", e);
        }

        if (user == null) {
            return Option.error(ERROR_USER_NOT_FOUND);
        }

        if (!passwordService.match(password, user.getPassword())) {
            return Option.error(ERROR_USERNAME_DOES_NOT_MATCH_PASSWORD);
        }

        return Option.success(user);
    }
}
