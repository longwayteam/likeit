package org.likeit.service;

import org.likeit.entity.Like;

import java.util.List;

public interface LikeService {

    List<Like> getAnswerLikes(Long answerId);

    long create(Like like);

    long update(Like like);

    Like get(long likeId);

    List<Like> getAll();

    void delete(long likeId);

    void addLikeToUser(long userId);

    void deleteLikeToUser(Like like);

    void delete(long userId, long answerId);
}
