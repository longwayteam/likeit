package org.likeit.controller;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class View {

    private String name;
    private Map<String, Object> attributes;
    private Map<String, Object> sessionAttributes;

    private View(String name) {
        this.name = name;
    }

    public static View jsp(String jspName) {
        return new View(jspName);
    }

    public static View redirect(String redirectUrl) {
        return new View("redirect:" + redirectUrl);
    }

    public String getRedirectUrl() {
        return name.substring(9);
    }

    public boolean doRedirect() {
        return name.contains("redirect");
    }

    public Map<String, Object> getSessionAttributes() {
        if (sessionAttributes == null) {
            return Collections.emptyMap();
        }
        return sessionAttributes;
    }

    public String getName() {
        return name;
    }

    public Map<String, Object> getAttributes() {
        if (attributes == null) {
            return Collections.emptyMap();
        }

        return attributes;
    }

    public void addAttribute(String name, Object value) {
        if (attributes == null) {
            attributes = new HashMap<>();
        }

        attributes.put(name, value);
    }

    public void addSessionAttribute(String name, Object value) {
        if (sessionAttributes == null) {
            sessionAttributes = new HashMap<>();
        }

        sessionAttributes.put(name, value);
    }
}
