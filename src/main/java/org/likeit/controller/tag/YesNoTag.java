package org.likeit.controller.tag;


import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class YesNoTag extends TagSupport {
    private static final Logger log = Logger.getLogger(YesNoTag.class);

    private boolean test;

    private Object yes;

    private Object no;

    public void setTest(boolean test) {
        this.test = test;
    }

    public void setYes(Object yes) {
        this.yes = yes;
    }

    public void setNo(Object no) {
        this.no = no;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();

        Object result = test ? yes : no;

        try {
            out.write((String)result);
        } catch (IOException e) {
            log.error("Exceptions happen!", e);
            throw  new JspException(e);
        }

        return SKIP_BODY;
    }
}
