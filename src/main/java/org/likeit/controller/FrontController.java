package org.likeit.controller;

import org.likeit.controller.action.Action;
import org.likeit.controller.action.Dispatcher;
import org.likeit.controller.action.Request;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/pages/*")
public class FrontController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest httpServletRequest, HttpServletResponse response)
            throws ServletException, IOException {

        String url = httpServletRequest.getPathInfo();
        String httpMethod = httpServletRequest.getMethod();

        Action action = Dispatcher.getAction(url, httpMethod);
        Request request = new Request(httpServletRequest);
        View view = action.process(request);

        setAttributes(httpServletRequest, view);

        if (view.doRedirect()) {
            response.sendRedirect(view.getRedirectUrl());
        } else {
            String path = "/WEB-INF/views/" + view.getName() + ".jsp";
            RequestDispatcher requestDispatcher = httpServletRequest.getRequestDispatcher(path);
            requestDispatcher.forward(httpServletRequest, response);
        }
    }

    private void setAttributes(HttpServletRequest httpServletRequest, View view){
        for (Map.Entry<String, Object> entry : view.getAttributes().entrySet()) {
            httpServletRequest.setAttribute(entry.getKey(), entry.getValue());
        }
        for (Map.Entry<String, Object> entry : view.getSessionAttributes().entrySet()) {
            httpServletRequest.getSession().setAttribute(entry.getKey(), entry.getValue());
        }
    }
}
