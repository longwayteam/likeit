package org.likeit.controller.action.user;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;

import javax.servlet.http.HttpServletRequest;

public class ActionShowLoginForm implements Action {

    @Override
    public View process(Request request) {
        return  View.jsp("user/login_page");
    }
}
