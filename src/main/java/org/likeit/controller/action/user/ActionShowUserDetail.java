package org.likeit.controller.action.user;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.dao.UserDao;
import org.likeit.dao.impl.UserDaoImpl;
import org.likeit.entity.User;

import javax.servlet.http.HttpServletRequest;

public class ActionShowUserDetail implements Action {

    private UserDao userDao;

    public ActionShowUserDetail() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public View process(Request request) {
        long userId = Long.parseLong(request.getParameter("id"));
        User user = userDao.findById(userId);

        if (user == null) {
            return View.jsp("error/404");
        }

        View view = View.jsp("user/user_page");
        view.addAttribute("user", user);

        return view;
    }
}
