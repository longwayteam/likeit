package org.likeit.controller.action.user;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.core.Err;
import org.likeit.core.Option;
import org.likeit.entity.UserRole;
import org.likeit.entity.User;
import org.likeit.service.UserService;
import org.likeit.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.likeit.service.UserService.*;

public class ActionCreateUser implements Action {

    private UserService userService;

    public ActionCreateUser() {
        this.userService = new UserServiceImpl();
    }

    @Override
    public View process(Request request) {
        User user = getUserFromRequest(request);

        Option<User> result;
        if (user.getId() == null) {
            result = userService.create(user);
        } else {
            result = userService.update(user);
        }

        if (result.hasErrors()) {
            View view = View.jsp("user/user_page");
            view.addAttribute("errors", transformToUiErrors(result.getErrors()));
            view.addAttribute("user", user);

            return view;
        }

        User createdUser = result.getObject();

        View view = View.redirect("/pages/userProfile?id=" + createdUser.getId());
        view.addSessionAttribute("authenticatedUser", createdUser);

        return view;
    }

    private User getUserFromRequest(Request request) {
        User user = new User();

        String userId = request.getParameter("id");

        if (userId != null) {
            user.setId(Long.parseLong(userId));
            user.setRole(UserRole.valueOf(request.getParameter("role")));
            user.setRating(request.getLongParameter("rating"));
        } else {
            user.setRating(0L);
            user.setRole(UserRole.USER);
        }

        user.setUserName(request.getParameter("userName"));
        user.setPassword(request.getParameter("password"));
        user.setFirstName(request.getParameter("firstName"));
        user.setLastName(request.getParameter("lastName"));


        return user;
    }

    private Map<String, String> transformToUiErrors(List<Err> errors) {
        Map<String, String> uiErrors = new HashMap<>();

        if (errors.contains(ERROR_PASSWORD_TOO_SMALL)) {
            uiErrors.put("password", "label.error.passwordToSmall");
        }

        if (errors.contains(ERROR_USERNAME_IS_EMPTY)) {
            uiErrors.put("emptyUsername", "label.error.emptyUserName");
        }

        if (errors.contains(ERROR_USERNAME_ALREADY_EXISTS)) {
            uiErrors.put("existsUsername", "label.error.userNameExists");
        }

        return uiErrors;
    }


}
