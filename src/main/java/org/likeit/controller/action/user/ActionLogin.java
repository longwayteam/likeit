package org.likeit.controller.action.user;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.core.Err;
import org.likeit.core.Option;
import org.likeit.entity.User;
import org.likeit.service.UserService;
import org.likeit.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActionLogin implements Action {

    private UserService userService;

    public ActionLogin() {
        this(new UserServiceImpl());
    }

    public ActionLogin(UserService userService) {
        this.userService = userService;
    }

    @Override
    public View process(Request request) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Option<User> result = userService.authenticate(username, password);

        if (result.hasErrors()) {
            View view = View.jsp("user/login_page");
            view.addAttribute("errors", transformToUiErrors(result.getErrors()));

            return view;
        }

        User user = result.getObject();
        View view = View.redirect("/pages/userProfile?id=" + user.getId());
        view.addSessionAttribute("authenticatedUser", user);

        return view;
    }

    private Map<String, String> transformToUiErrors(List<Err> errors) {
        Map<String, String> uiErrors = new HashMap<>();

        if (errors.contains(UserService.ERROR_USER_NOT_FOUND)) {
            uiErrors.put("username", "login.error.username");
        }

        if (errors.contains(UserService.ERROR_USERNAME_DOES_NOT_MATCH_PASSWORD)) {
            uiErrors.put("general", "login.error.general");
        }

        return uiErrors;
    }
}
