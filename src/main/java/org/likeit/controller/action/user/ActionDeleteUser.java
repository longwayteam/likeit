package org.likeit.controller.action.user;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.dao.UserDao;
import org.likeit.dao.impl.UserDaoImpl;

public class ActionDeleteUser implements Action {

    private UserDao userDao;

    public ActionDeleteUser() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public View process(Request request) {

        userDao.delete(request.getLongParameter("userId"));

        return View.redirect("/home");    }
}
