package org.likeit.controller.action.user;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.dao.UserDao;
import org.likeit.dao.impl.UserDaoImpl;
import org.likeit.entity.User;

public class ActionShowUserProfile implements Action {

    private UserDao userDao;

    public ActionShowUserProfile() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public View process(Request request) {
        Long userId = request.getLongParameter("id");

        User user = userDao.findById(userId);

        if (user == null) {
            return View.jsp("error/404");
        }

        View view = View.jsp("user/user_profile_page");
        view.addAttribute("user", user);

        return view;
    }
}
