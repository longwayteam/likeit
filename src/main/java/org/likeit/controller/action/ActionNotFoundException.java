package org.likeit.controller.action;

public class ActionNotFoundException extends RuntimeException {

    private String url;
    private String httpMethod;

    public ActionNotFoundException(String url, String httpMethod) {
        this.url = url;
        this.httpMethod = httpMethod;
    }

    @Override
    public String toString() {
        return String.format("ActionNotFoundException{url='%s', httpMethod='%s'}", url, httpMethod);
    }
}
