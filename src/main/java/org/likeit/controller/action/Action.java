package org.likeit.controller.action;


import org.likeit.controller.View;

import javax.servlet.http.HttpServletRequest;

public interface Action {

    View process(Request request);

}
