package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Question;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;

public class ActionShowUpdateForm implements Action {

    private QuestionService questionService;

    public ActionShowUpdateForm() {
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public View process(Request request) {
        Long questionId = request.getLongParameter("id");

        if (questionId == null) {
            return View.jsp("error/404");
        } else {
            Question question = questionService.get(questionId);
            View view = View.jsp("question/question_update");
            view.addAttribute("question", question);

            return view;
        }
    }
}
