package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;

import javax.servlet.http.HttpServletRequest;

public class ActionShowQuestionForm implements Action {
    @Override
    public View process(Request request) {
        return View.jsp("question/question_form");
    }
}
