package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Question;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ActionQuestionList implements Action {

    private QuestionService questionService;

    public ActionQuestionList() {
        this(new QuestionServiceImpl());
    }

    public ActionQuestionList(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public View process(Request request) {
        View view = View.jsp("question/list");

        List<Question> questions = questionService.getLimit(null, null);
        view.addAttribute("questions", questions);

        return view;
    }
}
