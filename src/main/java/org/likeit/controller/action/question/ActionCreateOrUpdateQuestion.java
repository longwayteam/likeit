package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Question;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ActionCreateOrUpdateQuestion implements Action {

    private QuestionService questionService;

    public ActionCreateOrUpdateQuestion() {
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public View process(Request request) {
        Question question = getQuestionFromRequest(request);

//        if(question.getId() != null) {
//            questionService.update(question);
//        } else {
           long questionId =  questionService.create(question);
           question.setUserId(questionId);
//        }

        View view = View.redirect("/pages/question?id=" + questionId);
        view.addAttribute("question", question);

        return view;
    }

    private Question getQuestionFromRequest(Request request) {
        Long questionId = request.getLongParameter("id");

        Question question = questionId == null ? new Question() : questionService.get(questionId);

        question.setTitle(request.getParameter("title"));
        question.setContent(request.getParameter("content"));
        question.setUserId(request.getLongParameter("userId"));

        return question;
    }
}
