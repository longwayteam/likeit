package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Question;
import org.likeit.service.QuestionService;
import org.likeit.service.UserService;
import org.likeit.service.impl.QuestionServiceImpl;
import org.likeit.service.impl.UserServiceImpl;

import java.util.List;


public class ActionShowUserQuestions implements Action {

    private QuestionService questionService;
    private UserService userService;

    public ActionShowUserQuestions() {
        this.questionService = new QuestionServiceImpl();
        this.userService = new UserServiceImpl();
    }

    @Override
    public View process(Request request) {
        long userId = request.getLongParameter("id");
        View view = View.jsp("question/question_user");

        List<Question> userQuestions = questionService.getUserQuestions(null, null, userId);

        view.addAttribute("questions", userQuestions);
        view.addAttribute("user", userService.get(userId));

        return view;
    }
}
