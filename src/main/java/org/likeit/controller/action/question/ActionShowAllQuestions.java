package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;
import org.likeit.service.pagination.PageRequest;

public class ActionShowAllQuestions implements Action {

    private QuestionService service;

    public ActionShowAllQuestions() {
        this.service = new QuestionServiceImpl();
    }

    @Override
    public View process(Request request) {

        long page = request.getLongParameter("page", 1);

        PageRequest pageRequest = new PageRequest(page, 7);

        View view = View.jsp("question/questions_page");
        view.addAttribute("page", service.getPage(pageRequest));

        return view;
    }
}
