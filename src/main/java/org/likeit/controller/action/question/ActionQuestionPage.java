package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Question;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ActionQuestionPage implements Action {

    private QuestionService questionService;

    public ActionQuestionPage() {
        this.questionService = new QuestionServiceImpl();
    }

    public ActionQuestionPage(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public View process(Request request) {

        View view = View.jsp("question/question_page");

        Long questionId = Long.parseLong(request.getParameter("id"));


        Question question = questionService.get(questionId);

        if (question == null) {
            return View.jsp("error/404");
        }

        view.addAttribute("question", question);

        return view;
    }
}
