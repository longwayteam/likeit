package org.likeit.controller.action.question;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;


public class ActionDeleteQuestion implements Action {
    private QuestionService questionService;

    public ActionDeleteQuestion() {
        this.questionService = new QuestionServiceImpl();
    }

    @Override
    public View process(Request request) {

        Long questionId = request.getLongParameter("questionId");

        questionService.delete(questionId);


        return View.redirect("/pages/allQuestions");
    }
}
