package org.likeit.controller.action;

import org.likeit.controller.View;

public class ActionLogout implements Action {
    @Override
    public View process(Request request) {

        request.getSession().invalidate();

        return View.redirect("/pages/home");
    }
}
