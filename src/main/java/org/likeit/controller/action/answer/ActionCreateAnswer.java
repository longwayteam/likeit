package org.likeit.controller.action.answer;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Answer;
import org.likeit.service.AnswerService;
import org.likeit.service.UserService;
import org.likeit.service.impl.AnswerServiceImpl;
import org.likeit.service.impl.UserServiceImpl;

public class ActionCreateAnswer implements Action {
    private AnswerService service;
    private UserService userService;

    public ActionCreateAnswer() {
        this.service = new AnswerServiceImpl();
        this.userService = new UserServiceImpl();
    }

    @Override
    public View process(Request request) {

        Answer answer = getAnswerFromRequest(request);
        Long answerId = service.create(answer);

        return View.redirect("/pages/question?id=" + answer.getQuestionId());
    }

    private Answer getAnswerFromRequest(Request request) {
        Answer answer = new Answer();

        answer.setContent(request.getParameter("content"));
        answer.setQuestionId(request.getLongParameter("questionId"));
        answer.setUser(userService.get(request.getLongParameter("userId")));

        return answer;
    }
}
