package org.likeit.controller.action.answer;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.service.AnswerService;
import org.likeit.service.impl.AnswerServiceImpl;

public class ActionDeleteAnswer implements Action {

    private AnswerService service;

    public ActionDeleteAnswer() {
        service = new AnswerServiceImpl();
    }

    @Override
    public View process(Request request) {

        Long answerId = request.getLongParameter("answerId");
        Long questionId = request.getLongParameter("questionId");

        service.delete(answerId);

        return View.redirect("/pages/question?id=" + questionId);
    }
}
