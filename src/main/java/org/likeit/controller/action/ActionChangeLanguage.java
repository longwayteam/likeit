package org.likeit.controller.action;

import org.likeit.controller.View;
import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

public class ActionChangeLanguage implements Action {
    @Override
    public View process(Request request) {
        String languageTag = request.getParameter("languageTag");

        Config.set(request.getSession(), Config.FMT_LOCALE, Locale.forLanguageTag(languageTag));

        return View.redirect(request.getHeader("referer"));
    }
}
