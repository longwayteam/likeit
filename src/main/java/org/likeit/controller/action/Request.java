package org.likeit.controller.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Request {

    private HttpServletRequest httpServletRequest;

    public Request(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    public String getParameter(String parameter) {
        return httpServletRequest.getParameter(parameter);
    }

    public Long getLongParameter(String parameter) {
        try {
            return Long.parseLong(httpServletRequest.getParameter(parameter));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public long getLongParameter(String parameter, long ifNull) {
        Long parameterValue = getLongParameter(parameter);

        return parameterValue != null ? parameterValue : ifNull;
    }

    public void removeSessionAttribute(String attribute) {
        getSession().removeAttribute(attribute);
    }

    public void setSessionAttribute(String name, Object value) {
        getSession().setAttribute(name, value);
    }

    public HttpSession getSession() {
        return httpServletRequest.getSession();
    }

    public String getHeader(String name) {
        return httpServletRequest.getHeader(name);
    }

}
