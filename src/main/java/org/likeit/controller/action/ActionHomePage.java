package org.likeit.controller.action;

import org.likeit.controller.View;
import org.likeit.service.QuestionService;
import org.likeit.service.impl.QuestionServiceImpl;

import javax.servlet.http.HttpServletRequest;

public class ActionHomePage implements Action {

    private QuestionService questionService;

    public ActionHomePage() {
        questionService = new QuestionServiceImpl();
    }

    @Override
    public View process(Request request) {
        View view = View.jsp("home");
        view.addAttribute("questions", questionService.getLimit(0L, 5L));

        return  view;
    }

}
