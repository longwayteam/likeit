package org.likeit.controller.action;

import org.likeit.controller.action.answer.ActionCreateAnswer;
import org.likeit.controller.action.answer.ActionDeleteAnswer;
import org.likeit.controller.action.like.ActionCreateLike;
import org.likeit.controller.action.like.ActionDeleteLike;
import org.likeit.controller.action.question.*;
import org.likeit.controller.action.user.*;

import java.util.ArrayList;
import java.util.List;

public class Dispatcher {

    private static List<ActionDefinition> ACTION_DEFINITIONS = new ArrayList<>();

    static {
        get("/questions", new ActionQuestionList());
        get("/home", new ActionHomePage());
        get("/question", new ActionQuestionPage());
        get("/login", new ActionShowLoginForm());
        get("/signup", new ActionShowRegistrationForm());
        post("/createUser", new ActionCreateUser());
        post("/updateUser", new ActionCreateUser());
        get("/userDetail", new ActionShowUserDetail());
        post("/login", new ActionLogin());
        get("/changeLanguage", new ActionChangeLanguage());
        get("/askQuestion", new ActionShowQuestionForm());
        get("/userDetail", new ActionShowUserDetail());
        post("/createQuestion", new ActionCreateOrUpdateQuestion());
        post("/createAnswer", new ActionCreateAnswer());
        post("/logout", new ActionLogout());
        get("/allQuestions", new ActionShowAllQuestions());
        post("/deleteAnswer", new ActionDeleteAnswer());
        post("/deleteQuestion", new ActionDeleteQuestion());
        post("/addLike", new ActionCreateLike());
        post("/deleteLike", new ActionDeleteLike());
        get("/userProfile", new ActionShowUserProfile());
        get("/updateQuestion", new ActionShowUpdateForm());
        post("/updateQuestion", new ActionCreateOrUpdateQuestion());
        get("/updateQuestionForm", new ActionShowUpdateForm());
        get("/getUserQuestions", new ActionShowUserQuestions());
        // TODO list of user questions
        //TODO: edit user details action
    }

    public static Action getAction(String url, String httpMethod) {
        for (ActionDefinition actionDefinition : ACTION_DEFINITIONS) {
            if (actionDefinition.match(url, httpMethod)) {
                return actionDefinition.getAction();
            }
        }

        throw new ActionNotFoundException(url, httpMethod);
    }

    private static void put(String url, Action action) {
        ACTION_DEFINITIONS.add(new ActionDefinition(url, "PUT", action));
    }

    private static void get(String url, Action action) {
        ACTION_DEFINITIONS.add(new ActionDefinition(url, "GET", action));
    }

    private static void post(String url, Action action) {
        ACTION_DEFINITIONS.add(new ActionDefinition(url, "POST", action));
    }

    private static void delete(String url, Action action) {
        ACTION_DEFINITIONS.add(new ActionDefinition(url, "DELETE", action));
    }

    private static class ActionDefinition {
        private String url;
        private String httpMethod;
        private Action action;

        public ActionDefinition(String url, String httpMethod, Action action) {
            this.url = url;
            this.httpMethod = httpMethod;
            this.action = action;
        }

        public boolean match(String url, String httpMethod) {

            return this.url.equals(url) && this.httpMethod.equals(httpMethod);
        }

        public String getUrl() {
            return url;
        }

        public String getHttpMethod() {
            return httpMethod;
        }

        public Action getAction() {
            return action;
        }
    }

}
