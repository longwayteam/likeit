package org.likeit.controller.action.like;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.dao.LikeDao;
import org.likeit.dao.impl.LikeDaoImpl;
import org.likeit.entity.Like;
import org.likeit.service.LikeService;
import org.likeit.service.impl.LikeServiceImpl;

public class ActionDeleteLike implements Action {

    private LikeService likeService;

    public ActionDeleteLike() {
        this.likeService = new LikeServiceImpl();
    }

    @Override
    public View process(Request request) {

        Long userId = request.getLongParameter("userId");
        Long answerId = request.getLongParameter("answerId");
        Long questionId = request.getLongParameter("questionId");

        Like like = new Like();

        like.setAnswerId(answerId);
        like.setUserId(userId);

        likeService.deleteLikeToUser(like);
        likeService.delete(userId, answerId);

        return View.redirect("/pages/question?id=" + questionId);
    }
}
