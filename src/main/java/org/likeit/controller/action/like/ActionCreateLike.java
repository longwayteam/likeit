package org.likeit.controller.action.like;

import org.likeit.controller.View;
import org.likeit.controller.action.Action;
import org.likeit.controller.action.Request;
import org.likeit.entity.Like;
import org.likeit.entity.User;
import org.likeit.service.LikeService;
import org.likeit.service.UserService;
import org.likeit.service.impl.LikeServiceImpl;

public class ActionCreateLike implements Action {

    private LikeService likeService;
    private UserService userService;

    public ActionCreateLike() {
        this.likeService = new LikeServiceImpl();
    }

    @Override
    public View process(Request request) {

        Like like = new Like();

        Long userId = request.getLongParameter("userId");

        like.setUserId(userId);
        like.setAnswerId(request.getLongParameter("answerId"));

        likeService.create(like);
        likeService.addLikeToUser(userId);



        Long questionId = request.getLongParameter("questionId");

        return View.redirect("/pages/question?id=" + questionId);
    }
}
