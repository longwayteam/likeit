package org.likeit.controller.action;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class RequestTest {

    @Mock
    private HttpServletRequest httpServletRequest;

    @InjectMocks
    private Request request;

    @Test
    public void getLongParameter_parameterIsNotNumber_resultIsNull() {
        // given
        doReturn("abc").when(httpServletRequest).getParameter("id");

        // when
        Long result = request.getLongParameter("id");

        // then
        assertNull(result);
    }

    @Test
    public void getLongParameter_parameterIsNumber_resultIsNumber() {
        // given
        doReturn("123").when(httpServletRequest).getParameter("id");

        // when
        Long result = request.getLongParameter("id");

        // then
        assertEquals(result, new Long(123));
    }

}
