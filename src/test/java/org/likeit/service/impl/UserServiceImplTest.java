package org.likeit.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.likeit.dao.DaoException;
import org.likeit.dao.UserDao;
import org.likeit.entity.User;
import org.likeit.service.PasswordService;
import org.likeit.service.ServiceException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserDao userDaoMock;

    @Mock
    private PasswordService passwordServiceMock;

    @Test
    public void get_parameterIsNumber_resultIsUser() {
        //given
        User userMock = new User();
        userMock.setId(1L);

        doReturn(userMock).when(userDaoMock).findById(1L);

        //when
        User user = userService.get(1L);

        //then
        assertSame(userMock, user);
    }

    @Test(expected = ServiceException.class)
    public void get_DaoExceptionIsThrown_resultIsServiceException() {
        //given
        doThrow(new DaoException("")).when(userDaoMock).findById(1L);

        //when
        userService.get(1L);

        //then
    }


}
